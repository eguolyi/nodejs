const express = require("express");
let app = new express();

app.get("/",function(req,res) {
    res.send("hello, node sample");
    
});

app.get("/about",function(req,res) {
    res.send("About web app");
    
});

let port = 12345;
app.listen(port, function() {
    console.log("Server started listening at localhost:" + port);
});